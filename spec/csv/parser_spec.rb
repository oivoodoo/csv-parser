RSpec.describe Csv::Parser do
  it "has a version number" do
    expect(Csv::Parser::VERSION).not_to be nil
  end

  it "should return empty array on using empty line" do
    expect(parse("")).to eq([])
  end

  describe 'example 0' do
    it 'should leave empty columns as well' do
      expect(parse(",\n,")).to eq([
        ['', ''],
        ['', ''],
      ])
    end

    it 'should leave empty columns as well' do
      expect(parse("a,b,\n,c,")).to eq([
        ['a', 'b', ''],
        ['', 'c', ''],
      ])
    end
  end

  describe 'example 1' do
    it 'should parse property csv line' do
      expect(parse("a,b,c\nd,e,f")).to eq([
        ["a", "b", "c"],
        ["d", "e", "f"],
      ])
    end
  end

  describe 'example 2' do
    it 'should parse property csv line' do
      expect(parse("one,\"two wraps,\nonto \"\"two\"\" lines\",three\n4,,6")).to eq(
        [
          ["one", "two wraps,\nonto \"two\" lines", "three"],
          ["4", "", "6"]
        ])
    end
  end

  describe 'example 3' do
    it 'should parse property csv line' do
      expect(parse("each\tword\tis\ta\tnew\tcolumn", "\t", nil)).to eq(
        [["each", "word", "is", "a", "new", "column"]])
    end
  end

  describe 'example 4' do
    it 'should parse property csv line' do
      expect(
        parse(
          "|the '\t' won't create new columns because it was|\tin\tquotes", "\t", "|")).to eq(
            [["the '\t' won't create new columns because it was", "in", "quotes"]])
    end
  end

  describe 'example 5' do
    it 'should parse property csv line' do
      expect(
        parse(
          "|alternate|\t|\"quote\"|\n\n|character|\t|hint: |||", "\t", "|")).to eq(
            [["alternate", "\"quote\""], [""], ["character", "hint: |"]])
    end
  end

  describe 'example 6' do
    it 'should parse property csv line' do
      expect(
        parse(
          "a,\n\n,c")).to eq(
            [["a", ""], [""], ["", "c"]])
    end
  end

  def parse(line, delimeter = ',', escape_char = '"')
    Csv::Parser.parse(line, delimeter, escape_char)
  end
end
