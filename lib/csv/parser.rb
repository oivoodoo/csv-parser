require "csv/parser/version"


module Csv

  module Parser
    NEW_LINE = "\n".freeze

    def self.parse(line, delim_char = ',', quote_char = '"')
      unless line.is_a?(String)
        raise ArgumentError('line argument should be String')
      end

      return [] if line.nil?

      line = line.strip
      return [] if line.empty?

      rows = []
      row = []
      buffer = ''

      while true
        if line.nil? || line.empty?
          row.push(buffer)
          buffer = ''
          rows.push(row)

          break
        end

        if line[0] == NEW_LINE
          row.push(buffer)
          buffer = ''
          rows.push(row)
          row = []

          line = line[1..-1]
          next
        end

        if line[0] == delim_char
          row.push(buffer)
          buffer = ''

          line = line[1..-1]
          next
        end

        # primary idea:
        # - parse columns first of all identifying quotes at the beginning
        # - then split logic into separate code

        # if no quotes ( 1 )
        if line[0] != quote_char
          index = [
            line.index(NEW_LINE),
            line.index(delim_char)
          ].compact.min

          if index && index != -1
            buffer = line[0..index-1]

            line = line[index..-1]
            next
          else
            buffer = line[0..-1]

            line = ''
            next
          end
        else # if quotes ( 2 )
          finished = false
          line = line[1..-1]
          while true
            break if line.nil? || line.empty? || finished

            index = line.index(quote_char)

            if index.nil? || index == -1
              raise ArgumentError.new("missing quotes for column")
            end

            case line[index + 1]
              when quote_char then
                sequence = 0
                while line[index+sequence+1] == quote_char
                  sequence += 1
                end

                buffer += line[0..index]
                line = line[index+sequence+1..-1]
              when delim_char then
                buffer += line[0..index-1]
                line = line[index+1..-1]
                finished = true
                next
              when NEW_LINE then
                buffer += line[0..index-1]
                line = line[index+1..-1]
                finished = true
                next
              when nil then
                buffer += line[0..index-1]
                line = ''
                finished = true
            end # case
          end # while
        end # else quotes
      end # while ...

      rows
    end
  end

end

